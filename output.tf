output "ecr_repositorr_url" {
    description = "The URL of the repository."
    value = aws_ecr_repository.my_ecr_repository.repository_url
}
output "registry_id" {
  description = "The account ID of the registry holding the repository."
  value = aws_ecr_repository.my_ecr_repository.registry_id
}